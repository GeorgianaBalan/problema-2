﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Problema_2
{
    class Program
    {
        static void Main(string[] args)
        {
            int n = int.Parse(Console.ReadLine());
            int[] vector = new int[n];

            for (int i = 0; i < n; i++)
                vector[i] = int.Parse(Console.ReadLine());
            int k = 0;
            int[] elem = new int[0];

            int a = int.Parse(Console.ReadLine());

            for (int i = 0; i < n; i++)
                if (vector[i] < a)
                {
                    Array.Resize(ref elem, elem.Length + 1);
                    elem[k] = vector[i];
                    k++;
                }
            Console.WriteLine("Numarul elementelor mai mici decat a este: " + k);
            for (int i = 0; i < k; i++)
            {
                Console.Write(elem[i] + " ");
            }


            Console.ReadKey();
        }
    }
}
